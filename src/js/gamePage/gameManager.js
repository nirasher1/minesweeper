import VisualBoard from "./board/visualBoard.js";
import {generateMinesPoints} from "./board/boardUtils/generateMinesUtils.js";
import { states as cellState } from "./board/cell/cellState.js";

const _winGame = Symbol("winGame");
const _loseGame = Symbol("loseGame");
const _isWinDetected = Symbol("isWinDetected");
const _isFirstMove = Symbol("isFirstMove");
const _onWin = Symbol("onWin");
const _onLose = Symbol("onLose");
const _firstMove = Symbol("firstMove");
const _checkForGameFinish = Symbol("checkForGameFinish");
const _handleGameFinish = Symbol("handleGameFinish");
const _placeMines = Symbol("placeMines");
const _onExpose = Symbol("onExpose");
const _increaseCountOfMinesToMark = Symbol("increaseCountOfMinesToMark");
const _decreaseCountOfMinesToMark = Symbol("decreaseCountOfMinesToMark");
const _onContextMenu = Symbol("onContextMenu");
const _watch = Symbol("watch");

export default class GameManager {
    constructor({
                    rowsCount,
                    columnsCount,
                    minesCount,
                    increaseCountOfMinesToMark,
                    decreaseCountOfMinesToMark,
                    onWin,
                    onLose,
                    watch
                } = {}) {
        this.board = new VisualBoard({
            rowsCount,
            columnsCount,
            minesCount,
            increaseCountOfMinesToMark,
            decreaseCountOfMinesToMark,
            onContextMenu: this[_onContextMenu],
            onClick: this[_checkForGameFinish],
            onExpose: this[_onExpose]
        });
        this[_increaseCountOfMinesToMark] = increaseCountOfMinesToMark
        this[_decreaseCountOfMinesToMark] = decreaseCountOfMinesToMark
        this[_isFirstMove] = true;
        this[_onWin] = onWin;
        this[_onLose] = onLose;
        this[_watch] = watch;
    }

    [_onContextMenu] = (currentCellState) => {
        if (currentCellState === cellState.hidden) {
            this[_decreaseCountOfMinesToMark]();
        }
        if (currentCellState === cellState.flagged) {
            this[_increaseCountOfMinesToMark]();
        }
    };

    [_placeMines] = clickedPoint => {
        const forbiddenPoints = this.board
            .getSurroundingPoints(clickedPoint)
            .concat([clickedPoint]);
        const minesPoints = generateMinesPoints(
            this.board.rowsCount,
            this.board.columnsCount,
            this.board.minesCount,
            forbiddenPoints
        );
        this.board.logicBoard.placeMines(minesPoints);
    };

    [_firstMove] = clickedPoint => {
        this[_watch].start();
        this[_placeMines](clickedPoint);
        this.board.logicBoard.calculateMinesCounts();
        this.board.logicBoard.cleanPointsToExpose = this.board.logicBoard.getCleanPoints();
    };

    [_handleGameFinish] = () => {
        this[_watch].stop();
    };

    [_winGame] = () => {
        this[_isWinDetected] = true;
        this.board.removeAllListeners();
        this[_onWin]();
    };

    [_loseGame] = () => {
        this.board.exposeUnexposedMines();
        this.board.exposeMistakeFlags();
        this.board.removeAllListeners();
        this[_onLose]();
    };

    [_checkForGameFinish] = clickedCell => {
        if (clickedCell.isMine) {
            this[_handleGameFinish]();
            this[_loseGame]();
        }
        if (this.board.logicBoard.cleanPointsToExpose.length === 0) {
            if (!this[_isWinDetected]) {
                this[_handleGameFinish]();
                this[_winGame]();
            }
        }
    };

    [_onExpose] = (clickedPoint) => {
        // This should be in onExpose because exposeCell opens other cells,
        // and the mines must be allocated before the recursive opening.
        if (this[_isFirstMove]) {
            this[_firstMove](clickedPoint);
            this[_isFirstMove] = false;
        }
    };
}