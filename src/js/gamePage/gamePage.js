import MessageModal from "../messageModal/messageModal.js"
import ControlPanel from "./controlPanel/controlPanel.js";
import { default as CONFIG } from "../../data/configuration.js"
import GameManager from "./gameManager.js"

const startANewGame = () => {
    location.reload();
};

const _finishGameAsWin = Symbol("finishGameAsWin");
const _finishGameAsLose = Symbol("finishGameAsLose");
const _controlPanel = Symbol("controlPanel");
const _onGameFinish = Symbol("finishGame");
const _gameManager = Symbol("gameManager");

export default class GamePage {
    constructor(rowsCount, columnsCount, minesCount) {
        this[_controlPanel] = new ControlPanel(minesCount, startANewGame);
        this[_gameManager] = new GameManager({
            rowsCount,
            columnsCount,
            minesCount,
            increaseCountOfMinesToMark: this[_controlPanel].increaseMinesToMarkCount.bind(this[_controlPanel]),
            decreaseCountOfMinesToMark: this[_controlPanel].decreaseMinesToMarkCount.bind(this[_controlPanel]),
            onWin: this[_finishGameAsWin],
            onLose: this[_finishGameAsLose],
            watch: this[_controlPanel].watch,
        });
    }

    [_onGameFinish] = (message) => {
        new MessageModal(
            message,
            true,
            [{
                title: "NEW GAME",
                onClick: startANewGame
            }]).render();
    };

    [_finishGameAsWin] = () => {
        this[_onGameFinish]("win");
    };

    [_finishGameAsLose] = () => {
        this[_onGameFinish]("lose");
    };

    render = () => {
        let board = document.createElement("div");
        board.classList.add(CONFIG.elementsIds.boardDivId);

        board.appendChild(this[_controlPanel].render());
        board.appendChild(this[_gameManager].board.render());

        document.body.appendChild(board);
    }
}