import Watch from "./watch.js";


const _element = Symbol("element");
const _watchElement = Symbol("watchElement");
const _minesCounterElement = Symbol("minesCounterElement");
const _newGameButtonElement = Symbol("element");
const _minesToMarkCount = Symbol("minesToMarkCount");
const _startANewGame = Symbol("startANewGame");
const _update = Symbol("update");

export default class ControlPanel {
    constructor(minesToMarkCount, startANewGame) {
        this.watch = new Watch();
        this[_minesToMarkCount] = minesToMarkCount;
        this[_startANewGame] = startANewGame;
        this[_element] = document.createElement("div");
        this[_watchElement] = document.createElement("span");
        this[_minesCounterElement] = document.createElement("span");
        this[_newGameButtonElement] = document.createElement("button");
    }

    [_update] = () => {
        this[_minesCounterElement].innerText = `mines: ${this[_minesToMarkCount]}`;
    }

    render = () => {
        const element = this[_element];
        element.classList.add("control-panel");

        this[_watchElement].appendChild(document.createTextNode("time: "));
        this[_watchElement].appendChild(this.watch.render());

        this[_newGameButtonElement].innerText = "START A NEW GAME";
        this[_newGameButtonElement].addEventListener("click", this[_startANewGame]);

        element.appendChild(this[_watchElement]);
        element.appendChild(this[_minesCounterElement]);
        element.appendChild(this[_newGameButtonElement]);

        this[_update]();
        return element
    };

    decreaseMinesToMarkCount = () => {
        this[_minesToMarkCount]--;
        this[_update]();
    };

    increaseMinesToMarkCount = () => {
        this[_minesToMarkCount]++;
        this[_update]();
    };
}