import CELL_BADGE from "../boardUtils/cellBadge.js";


const IMAGES_FOLDER_PATH = "src/images/";
const MISTAKE_MARK = "mistake-mark";
const FLAG_FILE_NAME = "flag.png";
const MINE_FILE_NAME = "mine.png";
const EXPOSED_CLASS = "exposed";
const NOT_EXPOSED_CLASS = "not-exposed";
const BOMBED_CLASS = "bombed";

const createImg = (fileName) => {
    const node = document.createElement("img");
    node.src = IMAGES_FOLDER_PATH + fileName;
    return node;
};

const createCountBadge = (count) => {
    const node = document.createElement("span");
    node.innerText = CELL_BADGE[count].text;
    node.style.color = CELL_BADGE[count].color;
    return node;
};

export const states = {
    flagged: {
        name: "flagged",
        getRendering: () => rendering.unexposed_flag(),
        onContext: 'questionMark',
        onClick: 'flagged',
    },
    exposed: {
        name: 'exposed',
        getRendering: (isMine, surroundingMinesCount) => isMine
            ? rendering.exposed_mine()
            : rendering.exposed_none(surroundingMinesCount),
        onContext: 'exposed',
        onClick: 'exposed',
    },
    hidden: {
        name: "hidden",
        getRendering: () => rendering.unexposed_none(),
        onContext: 'flagged',
        onClick: 'exposed',
    },
    questionMark: {
        name: "questionMark",
        getRendering: () => rendering.unexposed_questionMark(),
        onContext: 'hidden',
        onClick: 'questionMark',
    },
    bombed: {
        name: "bombed",
        getRendering: () => rendering.exposed_mine_bombed(),
        onContext: 'bombed',
        onClick: 'bombed',
    },
    mistakenFlagged: {
        name: "mistakenFlagged",
        getRendering: () => rendering.unexposed_flag_mistake(),
        onContext: "mistakenFlagged",
        onClick: "mistakenFlagged",
    }
}

const rendering = {
    unexposed_none: () => ({
        node: null,
        className: NOT_EXPOSED_CLASS,
    }),
    unexposed_flag: () => ({
        node: createImg(FLAG_FILE_NAME),
        className: NOT_EXPOSED_CLASS,
    }),
    unexposed_questionMark: () => ({
        node: createImg("question-mark.png"),
        className: NOT_EXPOSED_CLASS,
    }),
    unexposed_flag_mistake: () => ({
        node: (() => {
            const node = document.createElement("div");
            let imgNode = createImg(FLAG_FILE_NAME);
            const xText = document.createElement("div");
            xText.classList.add(MISTAKE_MARK);
            xText.innerText = "X";
            node.appendChild(imgNode);
            node.appendChild(xText);
            return node;
        })(),
        className: NOT_EXPOSED_CLASS,
    }),
    exposed_none: (surroundingMinesCount) => ({
        node: createCountBadge(surroundingMinesCount),
        className: EXPOSED_CLASS,
    }),
    exposed_mine: () => ({
        node: createImg(MINE_FILE_NAME),
        className: EXPOSED_CLASS,
    }),
    exposed_mine_bombed: () => ({
        node: createImg(MINE_FILE_NAME),
        className: `${EXPOSED_CLASS} ${BOMBED_CLASS}`,
    }),
}