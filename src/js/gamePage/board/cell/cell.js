import { states as cellState } from "./cellState.js";


const _element = Symbol("element");
const _onClick = Symbol("onClick");
const _handleContextMenu = Symbol("onContextMenu");
const _onContextMenuSignature = Symbol("onContextMenuSignature");
const _handleClick = Symbol("handleClick");
const _cleanCellExposureCleanup = Symbol("cleanCellExposureCleanup");
const _onExpose = Symbol("onExpose");
const _onContextMenu = Symbol("onContextMenu");

export default class Cell {
    constructor({
                    isMine = false,
                    minesAroundCount = 0,
                    onClick = () => {
                    },
                    cellExposureCleanup = () => {
                    },
                    onExpose = () => {
                    },
                    onContextMenu = () => {
                    }
                } = {}) {
        this.isMine = isMine;
        this.state = cellState.hidden;
        this.minesAroundCount = minesAroundCount;
        this[_onClick] = onClick;
        this[_cleanCellExposureCleanup] = cellExposureCleanup;
        this[_onExpose] = onExpose;
        this[_onContextMenu] = onContextMenu;
    }

    removeAllListeners = () => {
        this[_element].removeEventListener("click", this[_handleClick]);
        this[_element].removeEventListener("contextmenu", this[_onContextMenuSignature])
    };

    [_handleContextMenu] = (e) => {
        e.preventDefault();
        this[_onContextMenu](this.state);
        if (this.state === cellState.hidden || this.state  === cellState.flagged) {
            this[_element].removeEventListener("click", this[_handleClick]);
        } else {
            this[_element].addEventListener("click", this[_handleClick]);
        }
        this.state = cellState[this.state.onContext];
        this.update();
    };

    exposeCell = () => {
        if (this.state === cellState.flagged) {
            this.state = cellState.mistakenFlagged;
        } else {
            this.state = cellState.exposed;
        }
        this.update();
        this[_element].removeEventListener("click", this[_handleClick]);
        this[_onExpose]();
        if (!this.isMine) {
            this[_cleanCellExposureCleanup](this);
        } // TODO - merge all onExpose callback to array of callbacks?
    };

    [_handleClick] = () => {
        this.exposeCell();
        if (this.isMine) {
            this.markAsBombed();
        }
        this[_onClick](this);
    };

    markAsBombed = () => {
        this.state = cellState.bombed;
        this.update();
    };

    render = () => {
        this[_element] = document.createElement("td");
        this[_onContextMenuSignature] = this[_handleContextMenu].bind(this);
        this[_element].addEventListener("click", this[_handleClick]);
        this[_element].addEventListener("contextmenu", this[_onContextMenuSignature]);
        return this.update();
    }

    update = () => {
        let element = this[_element];
        element.innerHTML = '';

        const rendering = this.state.getRendering(this.isMine, this.minesAroundCount);
        element.className = rendering.className;
        rendering.node && element.appendChild(rendering.node);

        return element;
    }
}