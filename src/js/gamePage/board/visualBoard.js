import {states as cellState} from "./cell/cellState.js";
import LogicBoard from "./logicBoard.js";
import {getPointIndex} from "./boardUtils/pointUtils.js";
import {iterateMatrix} from "./boardUtils/boardUtils.js";
import { getSurroundingPoints } from "./boardUtils/pointUtils.js";


const BOARD_TABLE_CLASS = "board-table";
const _element = Symbol("element");

export default class VisualBoard {
    constructor({
                    rowsCount,
                    columnsCount,
                    minesCount,
                    onClick,
                    onExpose,
                    onContextMenu
    } = {}) {
        this.rowsCount = rowsCount;
        this.columnsCount = columnsCount;
        this.minesCount = minesCount;
        this.logicBoard = new LogicBoard({
            rowsCount,
            columnsCount,
            onClick,
            cleanCellExposureCleanup: this.clickCellWithNoMine,
            onExpose,
            onContextMenu
        });
        this[_element] = document.createElement("div");
    }

    getSurroundingPoints = clickedPoint => {
        return getSurroundingPoints(clickedPoint,
            this.rowsCount,
            this.columnsCount
        )
    };

    exposeSurroundingCells = (clickedCell, clickedCellPoint) => {
        if (!clickedCell.isMine && clickedCell.minesAroundCount === 0) {
            let surroundingPoints = this.getSurroundingPoints(clickedCellPoint);
            surroundingPoints.forEach(point => {
                const currentCell = this.logicBoard.cellsMatrix[point.rowIndex][point.columnIndex];
                if (currentCell.state === cellState.hidden) {
                    currentCell.exposeCell();
                    if (!clickedCell.isMine) {
                        this.exposeSurroundingCells(currentCell, point)
                    }
                }
            });
        }
    };

    exposeUnexposedMines = () => {
        this.logicBoard.minesPoints.forEach(point => {
            const currentCell = this.logicBoard.cellsMatrix[point.rowIndex][point.columnIndex];
            if (currentCell.state === cellState.hidden) {
                currentCell.exposeCell();
            }
        });
    };

    exposeMistakeFlags = () => {
        iterateMatrix(
            this.logicBoard.cellsMatrix,
            this.logicBoard.rowsCount,
            this.logicBoard.columnsCount,
            (cell) => {
            if (!cell.isMine && cell.state === cellState.flagged) {
                cell.exposeCell();
            }
        })
    };

    removeAllListeners = () => {
        iterateMatrix(
            this.logicBoard.cellsMatrix,
            this.logicBoard.rowsCount,
            this.logicBoard.columnsCount,
            cell => cell.removeAllListeners()
        );
    };

    clickCellWithNoMine = (clickedPoint, clickedCell) => {
        this.logicBoard.cleanPointsToExpose.splice(getPointIndex(clickedPoint, this.logicBoard.cleanPointsToExpose), 1);
        this.exposeSurroundingCells(clickedCell, clickedPoint);
    };
    
    render = () => {
        const tableElement = document.createElement("table");
        tableElement.classList.add(BOARD_TABLE_CLASS);
        this[_element].appendChild(tableElement);
        for (let rowIndex = 0; rowIndex < this.rowsCount; rowIndex++) {
            let currentRowElement = document.createElement("tr");
            tableElement.appendChild(currentRowElement);
            for (let columnIndex = 0; columnIndex < this.columnsCount; columnIndex++) {
                currentRowElement.appendChild(this.logicBoard.cellsMatrix[rowIndex][columnIndex].render());
            }
        }

        return this[_element];
    }
}