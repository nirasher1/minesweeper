import Cell from "./cell/cell.js";
import { getSurroundingPoints } from "./boardUtils/pointUtils.js";


const _createCellsMatrix = Symbol("createMatrixStructure");
const _onClick = Symbol("onClick");
const _calculateMinesCount = Symbol("calculateMinesCount");
const _cleanCellExposureCleanup = Symbol("cleanCellExposureCleanup");
const _onExpose = Symbol("onExpose");
const _onContextMenu = Symbol("onContextMenu");

export default class LogicBoard {
    constructor({
                    rowsCount,
                    columnsCount,
                    onClick,
                    cleanCellExposureCleanup,
                    onExpose,
                    onContextMenu
    } = {}) {
        this.rowsCount = rowsCount;
        this.columnsCount = columnsCount;
        this.minesPoints = [];
        this.cleanPointsToExpose = [];
        this[_onClick] = onClick;
        this[_cleanCellExposureCleanup] = cleanCellExposureCleanup;
        this[_onExpose] = onExpose;
        this[_onContextMenu] = onContextMenu;

        this[_createCellsMatrix]();
    }

    [_createCellsMatrix] = () => {
        const matrix = [];
        for (let rowIndex = 0; rowIndex < this.rowsCount; rowIndex++) {
            matrix.push([]);
            for (let columnIndex = 0; columnIndex < this.columnsCount; columnIndex++) {
                let currentPoint = { rowIndex, columnIndex }
                matrix[rowIndex].push(new Cell({
                    onClick: this[_onClick],
                    cellExposureCleanup: (cell) => this[_cleanCellExposureCleanup](currentPoint, cell),
                    onExpose: () => this[_onExpose](currentPoint),
                    onContextMenu: this[_onContextMenu]
                }));
            }
        }
        this.cellsMatrix = matrix;
    };

    placeMines = minesPoints => {
        this.minesPoints = minesPoints;
        this.minesPoints.forEach(minePoint => {
            this.cellsMatrix[minePoint.rowIndex][minePoint.columnIndex].isMine = true;
        })
    };

    [_calculateMinesCount] = (point) => {
        const pointsAround = getSurroundingPoints(point, this.rowsCount, this.columnsCount);
        let minesAround = 0;
        pointsAround.forEach(pointAround => {
            if (this.cellsMatrix[pointAround.rowIndex][pointAround.columnIndex].isMine) {
                minesAround++;
            }
        })
        this.cellsMatrix[point.rowIndex][point.columnIndex].minesAroundCount = minesAround;
    }

    calculateMinesCounts = () => {
        const rowsCount = this.rowsCount;
        const columnsCount = this.columnsCount;
        this.minesPoints.forEach((minePoint) => {
            let pointsAroundMinePoint = getSurroundingPoints(
                minePoint, rowsCount, columnsCount
            );
            pointsAroundMinePoint.forEach(pointAroundMine => {
                this[_calculateMinesCount](pointAroundMine);
            })
        })
    };

    getCleanPoints = () => {
        const cleanPoints = [];
        for (let rowIndex = 0; rowIndex < this.rowsCount; rowIndex++) {
            for (let columnIndex = 0; columnIndex < this.columnsCount; columnIndex++) {
                if (!this.cellsMatrix[rowIndex][columnIndex].isMine) {
                    cleanPoints.push({
                        rowIndex,
                        columnIndex
                    })
                }
            }
        }
        return cleanPoints
    };
}