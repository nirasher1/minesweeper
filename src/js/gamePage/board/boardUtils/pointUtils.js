const possibleSurroundingPoints = (centerPoint) => {
    const { rowIndex, columnIndex } = centerPoint;
    return [{
        rowIndex: rowIndex - 1,
        columnIndex: columnIndex - 1
    },
        {
            rowIndex: rowIndex - 1,
            columnIndex
        },
        {
            rowIndex: rowIndex - 1,
            columnIndex: columnIndex + 1
        },
        {
            rowIndex,
            columnIndex: columnIndex - 1
        },
        {
            rowIndex,
            columnIndex: columnIndex + 1
        },
        {
            rowIndex: rowIndex + 1,
            columnIndex: columnIndex - 1
        },
        {
            rowIndex: rowIndex + 1,
            columnIndex
        },
        {
            rowIndex: rowIndex + 1,
            columnIndex: columnIndex + 1
        }];
};

const isInBoundsPoint = (point, rowsCount, columnsCount) => {
    const { rowIndex, columnIndex } = point;
    return rowIndex >= 0 && rowIndex < rowsCount && columnIndex >= 0 && columnIndex < columnsCount;
};

export const getSurroundingPoints = (centerPoint, rowsCount, columnsCount) => {
    const existingPoints = [];

    const pointsToCheck = possibleSurroundingPoints(centerPoint);
    pointsToCheck.forEach(point => {
        if (isInBoundsPoint(point, rowsCount, columnsCount)) {
            existingPoints.push(point)
        }
    });

    return existingPoints;
};

export const getPointIndex = (generatedPoint, points) => {
    for (let index = 0; index < points.length; index++){
        let point = points[index];
        if (point.rowIndex === generatedPoint.rowIndex
            && point.columnIndex === generatedPoint.columnIndex) {
            return index;
        }
    }
    return -1;
};