const iterateMatrix = (matrix, rowsCount, columnsCount, callback) => {
    for (let rowIndex = 0; rowIndex < rowsCount; rowIndex++) {
        for (let columnIndex = 0; columnIndex < columnsCount; columnIndex++) {
            callback(matrix[rowIndex][columnIndex])
        }
    }
};

export { iterateMatrix };