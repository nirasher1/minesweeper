const CELL_BADGE = {
    0: {
        text: "",
    },
    1: {
        text: "1",
        color: "rgb(5, 5, 253)"
    },
    2: {
        text: "2",
        color: "rgb(17, 129, 17)"
    },
    3: {
        text: "3",
        color: "rgb(252, 6, 6)"
    },
    4: {
        text: "4",
        color: "rgb(0, 0, 123)"
    },
    5: {
        text: "5",
        color: "rgb(137, 1, 1)"
    },
    6: {
        text: "6",
        color: "rgb(0, 127, 128)"
    },
    7: {
        text: "7",
        color: "rgb(0, 0, 0)"
    },
    8: {
        text: "8",
        color: "rgb(127, 127, 127)"
    },
};

export default CELL_BADGE;