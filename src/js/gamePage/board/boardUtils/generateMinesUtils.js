const shuffle = array => {
    let remainingElementsToShuffleCount = array.length;
    let tempElementValue;
    let currentIndexToSwap;

    // While there remain elements to shuffle…
    while (remainingElementsToShuffleCount) {

        // Pick a remaining element…
        currentIndexToSwap = Math.floor(Math.random() * remainingElementsToShuffleCount--);

        // And swap it with the current element.
        tempElementValue = array[remainingElementsToShuffleCount];
        array[remainingElementsToShuffleCount] = array[currentIndexToSwap];
        array[currentIndexToSwap] = tempElementValue;
    }

    return array;
};

const calculateAcceptableMinesPoints = (rowsCount, columnsCount, skippedPoints) => {
    const acceptablePoints = [];
    for (let rowIndex = 0; rowIndex < rowsCount; rowIndex++) {
        for (let columnIndex = 0; columnIndex < columnsCount; columnIndex++) {
            const shouldSkip = skippedPoints.some(pointToSkip => {
                return rowIndex === pointToSkip.rowIndex && columnIndex === pointToSkip.columnIndex;
            });
            if (!shouldSkip) {
                acceptablePoints.push({
                    rowIndex,
                    columnIndex
                })
            }
        }
    }

    return acceptablePoints
};

export const generateMinesPoints = (rowsCount, columnsCount, minesCount, skippedPoints) => {
    const acceptableMinesPoints = calculateAcceptableMinesPoints(rowsCount, columnsCount, skippedPoints);
    const shuffledMinesPoints = shuffle(acceptableMinesPoints);
    return shuffledMinesPoints.slice(0, minesCount);
};