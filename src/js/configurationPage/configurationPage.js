import GameOption from "./gameOption/gameOption.js"
import CustomGameOption from "./gameOption/customGameOption.js"
import { default as CONFIG } from "../../data/configuration.js"

const _element = Symbol("element");
const _startButtonElement = Symbol("startButtonElement");
const _onOptionClick = Symbol("onOptionClick");
const _builtinOptionsMetadata = Symbol("builtinOptions");
const _customOptionMetadata = Symbol("customOption");
const _onStartGameClick = Symbol("onStartGameClick");
const _gameOptions = Symbol("gameOptions");
const _renderOptions = Symbol("renderOptions");

export default class ConfigurationPage {
    constructor({
                    builtinOptions,
                    minCustomRowsCount,
                    maxCustomRowsCount,
                    minCustomColumnsCount,
                    maxCustomColumnsCount,
                    minCustomMinesCount,
                    customOptionClassList,
                    onStartGameClick}) {
        this.selectedGameOption = null;
        this[_builtinOptionsMetadata] = builtinOptions;
        this[_customOptionMetadata] = {
            minCustomRowsCount,
            maxCustomRowsCount,
            minCustomColumnsCount,
            maxCustomColumnsCount,
            minCustomMinesCount,
            customOptionClassList
        };
        this[_onStartGameClick] = onStartGameClick;
        this[_gameOptions] = [];
        this[_element] = document.createElement("div");
        this[_startButtonElement] = document.createElement("button");
    }

    [_onOptionClick] = (e, gameOption) => {
        const currentSelectedOption = this.selectedGameOption;

        if (currentSelectedOption === null) {
            this[_startButtonElement].addEventListener("click",
                () => this[_onStartGameClick](this))
        }
        if (currentSelectedOption !== null && currentSelectedOption !== gameOption) {
            currentSelectedOption.unmarkAsSelectedOption();
        }

        this.selectedGameOption = gameOption;
        this.selectedGameOption.markAsSelectedOption();
    };

    [_renderOptions] = () => {
        const gameOptions = document.createElement("div");
        gameOptions.classList.add(CONFIG.elementsIds.gameOptionsDivId);

        // Built-in options
        this[_builtinOptionsMetadata].forEach(option => {
            const gameOption = new GameOption(option, (e, gameOption) => this[_onOptionClick](e, gameOption));
            this[_gameOptions].push(gameOption);
            gameOptions.appendChild(gameOption.render());
        });
        // Custom option
        const customGameOption = new CustomGameOption(this[_customOptionMetadata],
            (e, gameOption) => this[_onOptionClick](e, gameOption));
        gameOptions.appendChild(customGameOption.render());

        return gameOptions;
    };

    render = () => {
        this[_element].classList.add(CONFIG.elementsIds.configurationPageElementId);

        const title  = document.createElement("h2");
        title.innerText = "Choose your plan:";

        this[_startButtonElement].innerText = "START GAME";

        this[_element].appendChild(title);
        this[_element].appendChild(this[_renderOptions]());
        this[_element].appendChild(this[_startButtonElement]);

        document.body.appendChild(this[_element]);
    }

    unmount = () => {
        document.body.removeChild(this[_element]);
    }
}