const normalizeValue = (currentValue, minValue, maxValue) => {
    if (currentValue > maxValue) {
        return maxValue;
    } else if(currentValue < minValue && currentValue !== 0) {
        return minValue;
    }
    return currentValue
};

const getDisplayedValue = currentValue => {
    if (currentValue === 0) {
        return "";
    }
    return currentValue.toString();
}

const _onUpdate = Symbol("onUpdate");

export default class Input {
    constructor(minValue = 0, maxValue = 0, onUpdate = () => {}) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.currentValue = 0;
        this.currentDisplayedValue = "";
        this[_onUpdate] = onUpdate;
        this.element = document.createElement("input");
    }

    handleChange = () => {
        let elementValue = Math.round(parseInt(this.element.value));
        this.currentValue = normalizeValue(elementValue, this.minValue, this.maxValue);
        this.currentDisplayedValue = getDisplayedValue(this.currentValue);
        this.update();
    }

    update = () => {
        this.element.min = this.minValue.toString();
        this.element.max = this.maxValue.toString();
        this.element.value = this.currentDisplayedValue;
        this[_onUpdate]();
    };

    render = () => {
        this.element.setAttribute("type", "number");
        this.element.step = "1";

        this.update();

        this.element.addEventListener("blur", this.handleChange)
        return this.element;
    };
}