import GameOption from "./gameOption.js";
import Input from "./input.js"


export default class CustomGameOption extends GameOption {
    constructor({minCustomRowsCount,
                    maxCustomRowsCount,
                    minCustomColumnsCount,
                    maxCustomColumnsCount,
                    minCustomMinesCount,
                    customOptionClassList} = {}, onClick) {
        super({
            description: "Custom",
            rowsCount: null,
            columnsCount: null,
            minesCount: null,
            classList: customOptionClassList
        }, onClick);
        this.rowsCountInput = new Input(
            minCustomRowsCount,
            maxCustomRowsCount,
            this.onRowsCountUpdate
        )
        this.columnsCountInput = new Input(
            minCustomColumnsCount,
            maxCustomColumnsCount,
            this.onColumnsCountUpdate
        )
        this.minesCountInput = new Input(
            minCustomMinesCount,
            minCustomMinesCount,
            this.onMinesCountUpdate
        )

        this._rowsCountElement = this.rowsCountInput.render();
        this._columnsCountElement = this.columnsCountInput.render();
        this._minesCountElement = this.minesCountInput.render();
    }

    onRowsCountUpdate = () => {
        this.rowsCount = this.rowsCountInput.currentValue;
        this.normalizeMinesCount();
    }

    onColumnsCountUpdate = () => {
        this.columnsCount = this.columnsCountInput.currentValue;
        this.normalizeMinesCount();
    }

    onMinesCountUpdate = () => {
        this.minesCount = this.minesCountInput.currentValue;
    }

    normalizeMinesCount = () => {
        let currentRowsCount = this.rowsCountInput.currentValue || this.rowsCountInput.minValue;
        let currentColumnsCount = this.columnsCountInput.currentValue || this.columnsCountInput.minValue;
        this.minesCountInput.maxValue = (currentRowsCount - 1) * (currentColumnsCount - 1)
        this.minesCountInput.handleChange();
    }
}