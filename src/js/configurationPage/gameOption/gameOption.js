const SELECTED_CLASS = "selected";
const _description = Symbol("description");
const _onClick = Symbol("onClick");
const _element = Symbol("element");
const _renderConstAndColumnsCount = Symbol("renderConstAndColumnsCount");
const _renderMinesCount = Symbol("renderMinesCount");

export default class GameOption {
    constructor({description, rowsCount, columnsCount, minesCount, classList} = {}, onClick) {
        this.rowsCount = rowsCount;
        this.columnsCount = columnsCount;
        this.minesCount = minesCount;
        this._rowsCountElement = document.createTextNode(this.rowsCount);
        this._columnsCountElement = document.createTextNode(this.columnsCount);
        this._minesCountElement = document.createTextNode(this.minesCount);
        this[_description] = description;
        this[_onClick] = onClick;
        this[_element] = document.createElement("div");

        this[_element].classList.add(classList);
        this[_element].addEventListener("click", (e) => this[_onClick](e, this));
    }

    markAsSelectedOption = () => {
        this[_element].classList.add(SELECTED_CLASS);
    };

    unmarkAsSelectedOption = () => {
        this[_element].classList.remove(SELECTED_CLASS);
    };

    [_renderConstAndColumnsCount] = () => {
        const rowsAndColumnsCount = document.createElement("span");
        rowsAndColumnsCount.appendChild(this._rowsCountElement);
        rowsAndColumnsCount.appendChild(document.createTextNode(" X "));
        rowsAndColumnsCount.appendChild(this._columnsCountElement);
        return rowsAndColumnsCount;
    };

    [_renderMinesCount] = () => {
        const minesCount = document.createElement("span");
        minesCount.appendChild(this._minesCountElement);
        minesCount.appendChild(document.createTextNode(" mines"));
        return minesCount;
    };

    render = () => {
        this[_element].classList.add("game-option");

        this[_element].innerHTML = `
        <h2>${this[_description]}</h2>
        `;

        const fieldsElement = document.createElement("div");
        fieldsElement.appendChild(this[_renderConstAndColumnsCount]());
        fieldsElement.appendChild(this[_renderMinesCount]());
        this[_element].append(fieldsElement);

        return this[_element];
    }
}